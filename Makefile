R ?= Rscript --vanilla
JMETER ?= jmeter

.DEFAULT: pdf
pdf: report.rmd
	@$(R) -e 'rmarkdown::render("$<", "pdf_document", NULL, "outputs")'
	@rm report.acr # rmarkdown doesn't remove this one

jmeter: JU1 := 5
jmeter: JU2 := 5
jmeter: JR1 := $(shell expr 20 \* $(JU1))
jmeter: JR2 := $(shell expr 20 \* $(JU2))
jmeter: OUT := outputs/results-$(JU1)+$(JU2)
jmeter: skroutz-test-plan.jmx
	@printf 'Groups: $(JU1) users / $(JU2) users\nOutput: $(OUT)\n'
	@$(JMETER) -f -n -t $< -l$(OUT)/samples.jtl \
		-Jjmeter.save.saveservice.subresults=false \
		-Jgroup1.users=$(JU1) -Jgroup1.rampup=$(JR1) \
		-Jgroup2.users=$(JU2) -Jgroup2.rampup=$(JR2)
