# ---- summaries

# create a list of all the sample files
samples <- list.files("outputs", pattern = "results-*",
                      full.names = TRUE, recursive = FALSE)
samples <- paste(samples, "samples.jtl", sep = "/")
samples <- stringr::str_sort(samples, numeric = TRUE)

# define the column names for the summary file
# SD=Standard Deviation, TP=Throughput (per minute)
cols <- c("Page", "Avg.", "Min", "Max", "SD", "TP")

# create a summary file for each sample file
for (s in samples) {

  # read the sample file
  csv <- read.csv(s)[1:3]

  # get a list of unique labels
  lbls <- unique(csv$label)

  # create an empty data frame to store the summary
  summ <- matrix(ncol = 6, nrow = length(lbls) + 1)
  summ <- setNames(data.frame(summ), cols)

  # generate statistics for each label
  for (i in 1:length(lbls)) {

    # get the response times for this label
    res <- csv$elapsed[csv$label == lbls[i]]

    # also get the timestamps for this label
    times <- csv$timeStamp[csv$label == lbls[i]]

    # store the data in the summary
    summ[i, "Page"] <- lbls[i]
    summ[i, "Avg."] <- mean(res)
    summ[i, "Min"] <- min(res)
    summ[i, "Max"] <- max(res)
    summ[i, "SD"] <- sd(res)

    # throughput = responses / (last time - first time)
    # multiply by 6e4 to convert milliseconds to minutes
    summ[i, "TP"] <- length(times) /
      (max(times) - min(times)) * 6e4
  }

  # also store aggregate data
  summ[i + 1, "Page"] <- "TOTAL"
  summ[i + 1, "Avg."] <- mean(csv$elapsed)
  summ[i + 1, "Min"] <- min(csv$elapsed)
  summ[i + 1, "Max"] <- max(csv$elapsed)
  summ[i + 1, "SD"] <- sd(csv$elapsed)
  summ[i + 1, "TP"] <- length(csv$timeStamp) /
    (max(csv$timeStamp) - min(csv$timeStamp)) * 6e4

  # write results to summary file
  out <- sub("samples[.]jtl", "summary.csv", s)
  write.csv(summ, out, row.names = FALSE)
}

# ---- cdf

for (s in samples) {
  # create a CDF graph for each sample's response times
  plot(ecdf(read.csv(s)$elapsed), main = "",
       xlab = "", ylab = "", col = "navy")
}

# ---- boxplot

# read the response times from the sample file
elapsed <- read.csv("outputs/results-10+40/samples.jtl")$elapsed

# create a boxplot of the data and store the outliers
outliers <- boxplot(elapsed, horizontal = TRUE, pch = 4,
                    outcol = "maroon", col = "wheat")$out

# ---- histogram

# remove outliers from the data
elapsed <- elapsed[elapsed != outliers]



# create a histogram of the data
hist(elapsed, freq = FALSE, border = "blue",
     col = NULL, main = "", xlab = "", ylab = "")

# fit a normal distribution curve to the histogram
curve(dnorm(x, mean(elapsed), sd(elapsed)),
      col = "red", lwd = 2, add = TRUE)

# ---- shapiro-test

# perform Shapiro-Wilk test on the data
shapiro.test(elapsed)

# ---- confidence

# remove duplicate values
dedup <- unique(elapsed)

# 95% confidence level value
z <- qnorm(1 - 0.05 / 2)

# save mean, std dev, length
m <- mean(dedup)
s <- sd(dedup)
n <- length(dedup)


# calculate confidence interval
c1 <- m - z * s / sqrt(n)
c2 <- m + z * s / sqrt(n)
ci <- dedup[c1 <= dedup & dedup <= c2]

# perform Kolmogorov-Smirnoff test on the CI
ks.test(ci, "pnorm", mean(ci), sd(ci))

# ---- density

# plot the distribution of the CI
plot(density(ci, adjust = 2), col = "green",
     xlab = "", ylab = "", main = "", lwd = 2)
